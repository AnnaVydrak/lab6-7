﻿using System.Threading.Tasks;
using Microsoft.Playwright;

namespace lab6_7.PageObjects
{
    public class MainPageLogined : PageObjectBase
    {
        public readonly string UsernameSelector = "#nameofuser";

        private readonly ILocator _welcomingSign;

        public MainPageLogined(IPage page) : base(page)
        {
            _welcomingSign = page.Locator(UsernameSelector);
        }

        public async Task<string> GetUsernameAsync()
        {
            return await _welcomingSign.TextContentAsync();
        }
    }
}
